package io.github.mamifsidtect.goldengun.managers;

import io.github.mamifsidtect.goldengun.GoldenGun;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {

	private static ConfigManager arenas = new ConfigManager("arenas");
	private static ConfigManager lobbySigns = new ConfigManager("lobbysigns");

	public static ConfigManager getArenas() {
		return arenas;
	}

	public static ConfigManager getLobbySigns() {
		return lobbySigns;
	}

	/*****/

	private ConfigManager(String fileName) {
		System.out.println(GoldenGun.getPlugin());

		if (!GoldenGun.getPlugin().getDataFolder().exists()) GoldenGun.getPlugin().getDataFolder().mkdir();

		file = new File(GoldenGun.getPlugin().getDataFolder(), fileName + ".yml");

		if (!file.exists()) {
			try { file.createNewFile(); }
			catch (Exception e) { e.printStackTrace(); }
		}

		config = YamlConfiguration.loadConfiguration(file);
	}

	private File file;
	private FileConfiguration config;

	public void set(String path, Object value) {
		config.set(path, value);
		try { config.save(file); }
		catch (Exception e) { e.printStackTrace(); }
	}

	public ConfigurationSection createConfigurationSection(String path) {
		ConfigurationSection cs = config.createSection(path);
		try { config.save(file); }
		catch (Exception e) { e.printStackTrace(); }
		return cs;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String path) {
		return (T) config.get(path);
	}
}