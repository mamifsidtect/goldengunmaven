package io.github.mamifsidtect.goldengun.listeners;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.Arena;
import io.github.mamifsidtect.goldengun.managers.Arena.ArenaState;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.shampaggon.crackshot.CSUtility;

public class ClassSelectInventoryListener implements Listener {

	public final CSUtility csu = new CSUtility();
	private static HashMap<String, String> playerKit = new HashMap<String, String>();
	private static ClassSelectInventoryListener instance = new ClassSelectInventoryListener();
	
	public final ItemStack sniper = new ItemStack(Material.WOOD_HOE);{
		ItemMeta snipermeta = sniper.getItemMeta();
		ArrayList<String> sr = new ArrayList<String>();
		snipermeta.setDisplayName(ChatColor.YELLOW + "Sniper Loadout");
		sr.add("");
		sr.add(ChatColor.GOLD + "Primary Weapon: Winchester Repeater");
		sr.add(ChatColor.GOLD + "Secondary Weapon: Desert Eagle");
		sr.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenades");
		sr.add(ChatColor.GOLD + "Tactical Grenades: 1 Flashbang");
		snipermeta.setLore(sr);
		sniper.setItemMeta(snipermeta);
	}
	
	public final ItemStack hunter = new ItemStack(Material.GOLD_AXE);{
		ItemMeta huntermeta = hunter.getItemMeta();
		ArrayList<String> hr = new ArrayList<String>();
		huntermeta.setDisplayName(ChatColor.YELLOW + "Hunter Loadout");
		hr.add("");
		hr.add(ChatColor.GOLD + "Primary Weapon: M1014");
		hr.add(ChatColor.GOLD + "Secondary Weapon: M1911");
		hr.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenades");
		hr.add(ChatColor.GOLD + "Tactical Grenades: 1 Shock Charge");
		huntermeta.setLore(hr);
		hunter.setItemMeta(huntermeta);
	}
	
	public final ItemStack assault = new ItemStack(Material.IRON_SPADE);{
		ItemMeta assaultmeta = assault.getItemMeta();
		ArrayList<String> at = new ArrayList<String>();
		assaultmeta.setDisplayName(ChatColor.YELLOW + "Assault Loadout");
		at.add("");
		at.add(ChatColor.GOLD + "Primary Weapon: SCAR-H");
		at.add(ChatColor.GOLD + "Secondary Weapon: M1911");
		at.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenades");
		at.add(ChatColor.GOLD + "Tactical Grenades: 2 Flashbangs");
		assaultmeta.setLore(at);
		assault.setItemMeta(assaultmeta);
	}
	
	public final ItemStack marksman = new ItemStack(Material.GOLD_HOE);{
		ItemMeta marksmanmeta = marksman.getItemMeta();
		ArrayList<String> mn = new ArrayList<String>();
		marksmanmeta.setDisplayName(ChatColor.AQUA + "Marksman Loadout");
		mn.add("");
		mn.add(ChatColor.GOLD + "Primary Weapon: SV98");
		mn.add(ChatColor.GOLD + "Secondary Weapon: Python");
		mn.add(ChatColor.GOLD + "Tactical Grenade: 1 Flashbang");
		mn.add(ChatColor.GOLD + "Lethal Grenade: 1 Putty");
		marksmanmeta.setLore(mn);
		marksman.setItemMeta(marksmanmeta);
	}
	
	public final ItemStack predator = new ItemStack(Material.IRON_AXE);{
		ItemMeta predatormeta = predator.getItemMeta();
		ArrayList<String> pr = new ArrayList<String>();
		predatormeta.setDisplayName(ChatColor.AQUA + "Predator Loadout");
		pr.add("");
		pr.add(ChatColor.GOLD + "Primary Weapon: SPAS12");
		pr.add(ChatColor.GOLD + "Secondary Weapon: Desert Eagle");
		pr.add(ChatColor.GOLD + "Tactical Grenade: 1 Shock Charge");
		pr.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenade");
		predatormeta.setLore(pr);
		predator.setItemMeta(predatormeta);
	}
	
	public final ItemStack tactical = new ItemStack(Material.GOLD_SPADE);{
		ItemMeta tacmeta = tactical.getItemMeta();
		ArrayList<String> gr = new ArrayList<String>();
		tacmeta.setDisplayName(ChatColor.AQUA + "Tactical Loadout");
		gr.add("");
		gr.add(ChatColor.GOLD + "Primary Weapon: M16");
		gr.add(ChatColor.GOLD + "Secondary Weapon: Desert Eagle");
		gr.add(ChatColor.GOLD + "Lethal Grenade: 1 Grenade");
		gr.add(ChatColor.GOLD + "Tactical Grenades: 2 Shock Charges, 2 Flashbangs");
		tacmeta.setLore(gr);
		tactical.setItemMeta(tacmeta);
	}
	
	public final ItemStack assassin = new ItemStack(Material.DIAMOND_HOE);{
		ItemMeta assassinmeta = assassin.getItemMeta();
		ArrayList<String> an = new ArrayList<String>();
		assassinmeta.setDisplayName(ChatColor.GREEN + "Assassin Loadout");
		an.add("");
		an.add(ChatColor.GOLD + "Primary Weapon: XM500");
		an.add(ChatColor.GOLD + "Secondary Weapon: Python");
		an.add(ChatColor.GOLD + "Tactical Grenades: 2 Flashbangs, 1 Shock Charge");
		an.add(ChatColor.GOLD + "Lethal Grenade: 1 Grenade");
		assassinmeta.setLore(an);
		assassin.setItemMeta(assassinmeta);
	}
	
	public final ItemStack stalker = new ItemStack(Material.DIAMOND_AXE);{
		ItemMeta stalkermeta = stalker.getItemMeta();
		ArrayList<String> sr = new ArrayList<String>();
		stalkermeta.setDisplayName(ChatColor.GREEN + "Stalker Loadout");
		sr.add("");
		sr.add(ChatColor.GOLD + "Primary Weapon: Olympia");
		sr.add(ChatColor.GOLD + "Secondary Weapon: Python");
		sr.add(ChatColor.GOLD + "Tactical Grenades: 1 Flashbang, 1 Shock Charge");
		sr.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenades");
		stalkermeta.setLore(sr);
		stalker.setItemMeta(stalkermeta);
	}
	
	public final ItemStack soldier = new ItemStack(Material.DIAMOND_SPADE);{
		ItemMeta soldiermeta = soldier.getItemMeta();
		ArrayList<String> sor = new ArrayList<String>();
		soldiermeta.setDisplayName(ChatColor.GREEN + "Soldier Loadout");
		sor.add("");
		sor.add(ChatColor.GOLD + "Primary Weapon: AK47");
		sor.add(ChatColor.GOLD + "Secondary Weapon: Python");
		sor.add(ChatColor.GOLD + "Tactical Grenades: 1 Flashbang, 2 Shock Charges");
		sor.add(ChatColor.GOLD + "Lethal Grenade: 1 Putty");
		soldiermeta.setLore(sor);
		soldier.setItemMeta(soldiermeta);
	}
	
	public final ItemStack grenadier = new ItemStack(Material.DIAMOND_BARDING); {
		ItemMeta gmeta = grenadier.getItemMeta();
		ArrayList<String> g = new ArrayList<String>();
		gmeta.setDisplayName(ChatColor.GREEN + "Grenadier Loadout");
		g.add("");
		g.add(ChatColor.GOLD + "Primary Weapon: Grenade Launcher");
		g.add(ChatColor.GOLD + "Secondary Weapon: M1911");
		g.add(ChatColor.GOLD + "Tactical Grenades: 2 Flashbangs, 2 Shock Charges");
		gmeta.setLore(g);
		grenadier.setItemMeta(gmeta);
	}
	
	public ItemStack slugger = new ItemStack(Material.STONE_SPADE); {
		ItemMeta smeta = slugger.getItemMeta();
		ArrayList<String> s = new ArrayList<String>();
		smeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Slugger Loadout");
		s.add("");
		s.add(ChatColor.GOLD + "Primary Weapon: M26");
		s.add(ChatColor.GOLD + "Secondary Weapon: Handcannon");
		s.add(ChatColor.GOLD + "Tactical Grenades: 1 Shock Charge, 1 Flashbang");
		s.add(ChatColor.GOLD + "Lethal Grenades: 2 Grenades");
		smeta.setLore(s);
		slugger.setItemMeta(smeta);
	}
	
	public ItemStack heavy = new ItemStack(Material.IRON_HOE); {
		ItemMeta hmeta = heavy.getItemMeta();
		ArrayList<String> h = new ArrayList<String>();
		hmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Heavy Loadout");
		h.add("");
		h.add(ChatColor.GOLD + "Primary Weapon: Minigun");
		h.add(ChatColor.GOLD + "Secondary Weapon: M1911");
		h.add(ChatColor.GOLD + "Tactical Grenade: 2 Shock Charges");
		h.add(ChatColor.GOLD + "Lethal Grenade: 2 Grenades");
		hmeta.setLore(h);
		heavy.setItemMeta(hmeta);
	}
	
	public final ItemStack iingot = new ItemStack(Material.IRON_INGOT, 1);{
		ItemMeta iimeta = iingot.getItemMeta();
		ArrayList<String> i = new ArrayList<String>();
		iimeta.setDisplayName(ChatColor.AQUA + "Plus Loadouts");
		i.add("");
		i.add(ChatColor.GOLD + "If you do not have any of these loadouts");
		i.add(ChatColor.GOLD + "you can purchase them in the dogtag shop");
		i.add(ChatColor.GOLD + "or unlock it immediately by purchasing a rank");
		i.add(ChatColor.GOLD + "at http://mcthewarzone.com/shop");
		iimeta.setLore(i);
		iingot.setItemMeta(iimeta);
	}
	
	public final ItemStack gingot = new ItemStack(Material.GOLD_INGOT, 1);{
		ItemMeta gimeta = gingot.getItemMeta();
		ArrayList<String> g = new ArrayList<String>();
		gimeta.setDisplayName(ChatColor.GREEN + "Premium Loadouts");
		g.add("");
		g.add(ChatColor.GOLD + "If you do not have any of these loadouts");
		g.add(ChatColor.GOLD + "you can purchase them in the dogtag shop");
		g.add(ChatColor.GOLD + "or unlock it immediately by purchasing a rank");
		g.add(ChatColor.GOLD + "at http://mcthewarzone.com/shop");
		gimeta.setLore(g);
		gingot.setItemMeta(gimeta);
	}
	
	public final ItemStack dogtags = new ItemStack(Material.NAME_TAG, 1); {
		ItemMeta dtmeta = dogtags.getItemMeta();
		ArrayList<String> dt = new ArrayList<String>();
		dtmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Dogtag Classes");
		dt.add("");
		dt.add(ChatColor.GOLD + "If you do not have any of these loadouts");
		dt.add(ChatColor.GOLD + "Purchase them in the Dogtag Shop");
		dtmeta.setLore(dt);
		dogtags.setItemMeta(dtmeta);
	}
	
	public final ItemStack brick = new ItemStack(Material.CLAY_BRICK, 1);{
		ItemMeta bmeta = brick.getItemMeta();
		ArrayList<String> b = new ArrayList<String>();
		bmeta.setDisplayName(ChatColor.YELLOW + "Default Classes");
		b.add("");
		b.add(ChatColor.GOLD + "Everyone can access these classes!");
		bmeta.setLore(b);
		brick.setItemMeta(bmeta);
	}
	
	public static ClassSelectInventoryListener getInstance() {
		return instance;
	}
	
	public HashMap<String, String> getPlayerKitMap() {
		return playerKit;
	}
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (ArenaManager.getInstance().getArena(p) != null && ArenaManager.getInstance().getArena(p).getState() != ArenaState.STARTED) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (p.getItemInHand().equals(Arena.classselect)) {
					p.openInventory(GoldenGun.classSelector);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerMainInventoryClick(InventoryClickEvent event) {
		Player clicker = (Player) event.getWhoClicked();
		if(event.getInventory().equals(GoldenGun.classSelector)) {
			if (event.getCurrentItem().equals(brick)) {
				clicker.openInventory(GoldenGun.defaultClassSelector);
				event.setCancelled(true);
			} else if (event.getCurrentItem().equals(iingot)) {
				clicker.openInventory(GoldenGun.chiefClassSelector);
				event.setCancelled(true);
			} else if (event.getCurrentItem().equals(gingot)) {
				clicker.openInventory(GoldenGun.generalClassSelector);
				event.setCancelled(true);
			} else if (event.getCurrentItem().equals(dogtags)) {
				clicker.openInventory(GoldenGun.dogtagClassSelector);
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerDefaultInventoryClick(InventoryClickEvent event) {
		Player clicker = (Player) event.getWhoClicked();
		if (event.getInventory().equals(GoldenGun.defaultClassSelector)) {
			if (event.getCurrentItem().equals(sniper)) {
				clicker.getInventory().clear();
				csu.giveWeapon(clicker, "Winchester", 1);
				csu.giveWeapon(clicker, "Grenade", 2);
				csu.giveWeapon(clicker, "Flashbang", 1);
				csu.giveWeapon(clicker, "Eagle", 1);
				clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
				getPlayerKitMap().put(clicker.getName(), "Sniper");
				clicker.sendMessage(ChatColor.YELLOW + "You are now using the Sniper kit");
				event.setCancelled(true);
				clicker.closeInventory();
			} else if (event.getCurrentItem().equals(hunter)) {
				clicker.getInventory().clear();
				csu.giveWeapon(clicker, "M1014", 1);
				csu.giveWeapon(clicker, "Grenade", 2);
				csu.giveWeapon(clicker, "Shock", 1);
				csu.giveWeapon(clicker, "M1911", 1);
				clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
				getPlayerKitMap().put(clicker.getName(), "Hunter");
				clicker.sendMessage(ChatColor.YELLOW + "You are now using the Hunter kit");
				event.setCancelled(true);
				clicker.closeInventory();
			} else if (event.getCurrentItem().equals(assault)) {
				clicker.getInventory().clear();
				csu.giveWeapon(clicker, "SCAR", 1);
				csu.giveWeapon(clicker, "Grenade", 2);
				csu.giveWeapon(clicker, "Flashbang", 2);
				csu.giveWeapon(clicker, "M1911", 1);
				clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
				getPlayerKitMap().put(clicker.getName(), "Assault");
				clicker.sendMessage(ChatColor.YELLOW + "You are now using the Assault kit");
				event.setCancelled(true);
				clicker.closeInventory();
			}
		}
	}
	
	@EventHandler
	public void onPlayerChiefInventoryClick(InventoryClickEvent event) {
		Player clicker = (Player) event.getWhoClicked();
		if (event.getInventory().equals(GoldenGun.chiefClassSelector)) {
			if (event.getCurrentItem().equals(marksman)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.marksman")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "SV98", 1);
					csu.giveWeapon(clicker, "Putty", 1);
					csu.giveWeapon(clicker, "Flashbang", 1);
					csu.giveWeapon(clicker, "Python", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Marksman");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Marksman kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			} else if (event.getCurrentItem().equals(predator)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.predator")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "SPAS12", 1);
					csu.giveWeapon(clicker, "Shock", 1);
					csu.giveWeapon(clicker, "Grenade", 2);
					csu.giveWeapon(clicker, "Eagle", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Predator");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Predator kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			} else if (event.getCurrentItem().equals(tactical)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.tactical")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "M16", 1);
					csu.giveWeapon(clicker, "Flashbang", 2);
					csu.giveWeapon(clicker, "Shock", 2);
					csu.giveWeapon(clicker, "Grenade", 1);
					csu.giveWeapon(clicker, "Eagle", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Tactical");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Tactical kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerGeneralInventoryClick(InventoryClickEvent event) {
		Player clicker = (Player) event.getWhoClicked();
		if (event.getInventory().equals(GoldenGun.generalClassSelector)) {
			if (event.getCurrentItem().equals(assassin)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.assassin")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "XM500", 1);
					csu.giveWeapon(clicker, "Putty", 1);
					csu.giveWeapon(clicker, "Flashbang", 2);
					csu.giveWeapon(clicker, "Shock", 1);
					csu.giveWeapon(clicker, "Python", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Assassin");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Assassin kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			} else if (event.getCurrentItem().equals(stalker)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.stalker")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "Olympia", 1);
					csu.giveWeapon(clicker, "Flashbang", 1);
					csu.giveWeapon(clicker, "Grenade", 2);
					csu.giveWeapon(clicker, "Shock", 1);
					csu.giveWeapon(clicker, "Python", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Stalker");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Stalker kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			} else if (event.getCurrentItem().equals(soldier)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.soldier")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "AK47", 1);
					csu.giveWeapon(clicker, "Flashbang", 1);
					csu.giveWeapon(clicker, "Shock", 2);
					csu.giveWeapon(clicker, "Putty", 1);
					csu.giveWeapon(clicker, "Python", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Soldier");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Soldier kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			} else if (event.getCurrentItem().equals(grenadier)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.grenadier")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "Launcher", 1);
					csu.giveWeapon(clicker, "Flashbang", 2);
					csu.giveWeapon(clicker, "Shock", 2);
					csu.giveWeapon(clicker, "M1911", 1);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Grenadier");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Grenadier kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDogtagInventoryClick(InventoryClickEvent event) {
		Player clicker = (Player) event.getWhoClicked();
		if (event.getInventory().equals(GoldenGun.dogtagClassSelector)) {
			if (event.getCurrentItem().equals(slugger)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.slugger")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "M26", 1);
					csu.giveWeapon(clicker, "Handcannon", 1);
					csu.giveWeapon(clicker, "Shock", 1);
					csu.giveWeapon(clicker, "Flashbang", 1);
					csu.giveWeapon(clicker, "Grenade", 2);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Slugger");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Slugger kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}	
			} else if (event.getCurrentItem().equals(heavy)) {
				if (GoldenGun.perms.playerHas(clicker, "goldengun.kits.heavy")) {
					clicker.getInventory().clear();
					csu.giveWeapon(clicker, "Minigun", 1);
					csu.giveWeapon(clicker, "M1911", 1);
					csu.giveWeapon(clicker, "Grenade", 2);
					csu.giveWeapon(clicker, "Shock", 2);
					clicker.getInventory().setItem(8, new ItemStack(Arena.classselect));
					getPlayerKitMap().put(clicker.getName(), "Heavy");
					clicker.sendMessage(ChatColor.YELLOW + "You are now using the Heavy kit");
					event.setCancelled(true);
					clicker.closeInventory();
				} else {
					MessageManager.getInstance().msg(clicker, MessageType.BAD, "You have not bought this class!");
					event.setCancelled(true);
				}
			}
		}
	}
}
