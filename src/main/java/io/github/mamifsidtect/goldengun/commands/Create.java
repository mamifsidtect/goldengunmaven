package io.github.mamifsidtect.goldengun.commands;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.ConfigManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Create extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (GoldenGun.perms.has(p, "goldengun.commands.create")) {
			int id = ArenaManager.getInstance().getArenas().size() + 1;

			ConfigManager.getArenas().createConfigurationSection("arenas." + id);
			ConfigManager.getArenas().set("arenas." + id + ".numPlayers", 10);
			ConfigManager.getArenas().set("arenas." + id + ".minPlayers", 4);
			ConfigManager.getArenas().set("arenas." + id + ".mapName", "Default Name");

			MessageManager.getInstance().msg(p, MessageType.GOOD, "Created Arena " + id + "!");

			ArenaManager.getInstance().setupArenas();
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public Create() {
		super("Create an arena, ", "", "create");
	}
}