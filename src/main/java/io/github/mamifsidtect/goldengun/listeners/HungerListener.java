package io.github.mamifsidtect.goldengun.listeners;

import io.github.mamifsidtect.goldengun.managers.ArenaManager;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class HungerListener implements Listener {

	@EventHandler
	public void onPlayerFoodLevelChange(FoodLevelChangeEvent event) {
		HumanEntity e = event.getEntity();
		if (e instanceof Player) {
			Player p = (Player) event.getEntity();
			if (ArenaManager.getInstance().getArena(p) != null) {
				event.setCancelled(true);
			}
		}
	}	
}
