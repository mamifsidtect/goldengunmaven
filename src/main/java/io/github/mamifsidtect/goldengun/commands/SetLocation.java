package io.github.mamifsidtect.goldengun.commands;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.ConfigManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SetLocation extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (GoldenGun.perms.playerHas(p, "goldengun.commands.setloc")) {
			if (args.length == 0) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You did not specify an arena ID.");
				return;
			}
			
			int id = -1;

			try { id = Integer.parseInt(args[0]); }
			catch (Exception e) {
				MessageManager.getInstance().msg(p, MessageType.BAD, args[0] + " is not a valid number!");
				return;
			}

			if (ConfigManager.getArenas().<ConfigurationSection>get("arenas." + id) == null) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "There is no arena with ID " + id + "!");
				return;
			}

			ConfigurationSection s = ConfigManager.getArenas().createConfigurationSection("arenas." + id + ".spawn");

			s.set("world", p.getWorld().getName());
			s.set("x", p.getLocation().getX());
			s.set("y", p.getLocation().getY());
			s.set("z", p.getLocation().getZ());
			s.set("yaw", p.getLocation().getYaw());
			s.set("pitch", p.getLocation().getPitch());

			ConfigManager.getArenas().set("arenas." + id + ".spawn", s);

			ArenaManager.getInstance().setupArenas();

			MessageManager.getInstance().msg(p, MessageType.GOOD, "Set spawn for arena " + id + "!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public SetLocation() {
		super("Set the spawn location.", "<id>", "sloc");
	}
}