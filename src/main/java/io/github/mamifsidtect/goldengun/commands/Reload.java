package io.github.mamifsidtect.goldengun.commands;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Reload extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (GoldenGun.perms.playerHas(p, "goldengun.commands.reload")) {
			ArenaManager.getInstance().setupArenas();
			MessageManager.getInstance().msg(p, MessageType.GOOD, "Reloaded goldengun!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public Reload() {
		super("Reload the arenas.", "", "reload");
	}
}