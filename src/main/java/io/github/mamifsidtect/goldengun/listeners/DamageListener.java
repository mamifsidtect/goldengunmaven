package io.github.mamifsidtect.goldengun.listeners;

import io.github.mamifsidtect.goldengun.managers.Arena.ArenaState;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		Entity e = event.getEntity();
		if (e instanceof Player) {			
			if (ArenaManager.getInstance().getArena((Player) e) != null) {
				if (ArenaManager.getInstance().getArena((Player) e).getState() == ArenaState.STARTED) {
					event.setCancelled(false);
				} else {
					event.setCancelled(true);
				}
			}
		}
	}
}
