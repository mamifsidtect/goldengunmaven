package io.github.mamifsidtect.goldengun;

import io.github.mamifsidtect.goldengun.listeners.ClassSelectInventoryListener;
import io.github.mamifsidtect.goldengun.listeners.DamageListener;
import io.github.mamifsidtect.goldengun.listeners.GameListener;
import io.github.mamifsidtect.goldengun.listeners.HungerListener;
import io.github.mamifsidtect.goldengun.listeners.JoinListener;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.CommandManager;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class GoldenGun extends JavaPlugin {
	
	public final ClassSelectInventoryListener csil = new ClassSelectInventoryListener();
	public final ArenaManager am = new ArenaManager();
	
	public static Permission perms = null;
	public static Economy econ = null;
	
	Logger logger = Logger.getLogger("Minecraft");
	
	public static Inventory classSelector;
	public static Inventory defaultClassSelector;
	public static Inventory chiefClassSelector;
	public static Inventory generalClassSelector;
	public static Inventory dogtagClassSelector;
	
	public static Plugin getPlugin() {
		return Bukkit.getServer().getPluginManager().getPlugin("GoldenGun");
	}

	@Override
	public void onEnable() {
		PluginDescriptionFile pdfile = this.getDescription();
		logger.info(pdfile.getName() + " Version: " + pdfile.getVersion() + " is now enabled!");
		
		ArenaManager.getInstance().setupArenas();
		
		setupPermissions();
		setupEconomy();

		classSelector = Bukkit.createInventory(null, 9, ChatColor.WHITE + "Class Selector"); {
			classSelector.setItem(1, new ItemStack(csil.brick));
			classSelector.setItem(3, new ItemStack(csil.iingot));
			classSelector.setItem(5, new ItemStack(csil.gingot));
			classSelector.setItem(7, new ItemStack(csil.dogtags));
		}		
		defaultClassSelector = Bukkit.createInventory(null, 9, ChatColor.WHITE + "Class Selector"); {
			defaultClassSelector.setItem(1, new ItemStack(csil.sniper));
			defaultClassSelector.setItem(4, new ItemStack(csil.hunter));
			defaultClassSelector.setItem(7, new ItemStack(csil.assault));
		}		
		chiefClassSelector = Bukkit.createInventory(null, 9, ChatColor.WHITE + "Class Selector"); {
			chiefClassSelector.setItem(1, new ItemStack(csil.marksman));
			chiefClassSelector.setItem(4, new ItemStack(csil.predator));
			chiefClassSelector.setItem(7, new ItemStack(csil.tactical));
		}		
		generalClassSelector = Bukkit.createInventory(null, 9, ChatColor.WHITE + "Class Selector"); {
			generalClassSelector.setItem(1, new ItemStack(csil.assassin));
			generalClassSelector.setItem(3, new ItemStack(csil.stalker));
			generalClassSelector.setItem(5, new ItemStack(csil.soldier));
			generalClassSelector.setItem(7, new ItemStack(csil.grenadier));
		}
		
		dogtagClassSelector = Bukkit.createInventory(null, 9, ChatColor.WHITE + "Class Selector"); {
			dogtagClassSelector.setItem(2, new ItemStack(csil.slugger));
			dogtagClassSelector.setItem(6, new ItemStack(csil.heavy));
		}
		
		Bukkit.getPluginManager().registerEvents(new GameListener(), this);
		Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new HungerListener(), this);
		Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new ClassSelectInventoryListener(), this);
		//Bukkit.getPluginManager().registerEvents(new SignListener(), this);
		
		CommandManager cm = new CommandManager();
		cm.setup();
		getCommand("goldengun").setExecutor(cm);
	}
	
	@Override
	public void onDisable() {
		PluginDescriptionFile pdfile = this.getDescription();
		logger.info(pdfile.getName() + " Version: " + pdfile.getVersion() + " is now disabled!");
		
		GameListener.getInstance().getPlayerKillsMap().clear();
		ClassSelectInventoryListener.getInstance().getPlayerKitMap().clear();
	}
	
	public ArenaManager getArenaManager() {
		return am;
	}
	
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
    
    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            econ = economyProvider.getProvider();
        }

        return (econ != null);
    }
}
