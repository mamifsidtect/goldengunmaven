package io.github.mamifsidtect.goldengun.managers;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.listeners.ClassSelectInventoryListener;
import io.github.mamifsidtect.goldengun.listeners.GameListener;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;
import io.github.mamifsidtect.warzonehub.listeners.ScoreboardListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.shampaggon.crackshot.CSUtility;

public class Arena implements Listener {
	
	public enum ArenaState { DISABLED, WAITING, COUNTING_DOWN, FINISHING, STARTED; }
	
	public static ItemStack classselect = new ItemStack(Material.STICK); {
		ItemMeta csmeta = classselect.getItemMeta();
		ArrayList<String> cs = new ArrayList<String>();
		csmeta.setDisplayName(ChatColor.YELLOW + "Class Selection (Right-Click)");
		cs.add("");
		cs.add(ChatColor.GOLD + "Right-click to choose your class");
		csmeta.setLore(cs);
		classselect.setItemMeta(csmeta);
	}
	
	public final CSUtility csu = new CSUtility();
	
	public int id, taskID, numPlayers, currentPlayers, minPlayers = 0;
	protected ArenaState state = ArenaState.DISABLED;
	public ArrayList<PlayerData> data;
	public Location spawnPoint; 
	public String mapName;

	class ValueComparator implements Comparator<String> {

	    Map<String, Integer> base;
	    public ValueComparator(Map<String, Integer> base) {
	        this.base = base;
	    }

	    // Note: this comparator imposes orderings that are inconsistent with equals.    
	    public int compare(String a, String b) {
	        if (base.get(a) >= base.get(b)) {
	            return -1;
	        } else {
	            return 1;
	        } // returning 0 would merge keys
	    }
	}
	
	public Arena(int id) {
		this.id = id;
		this.data = new ArrayList<PlayerData>();
		this.numPlayers = ConfigManager.getArenas().get("arenas." + id + ".numPlayers");
		this.minPlayers = ConfigManager.getArenas().get("arenas." + id + ".minPlayers");
		this.mapName = ConfigManager.getArenas().get("arenas." + id + ".mapName");
		
		ConfigurationSection s = ConfigManager.getArenas().get("arenas." + id + ".spawn");
		
		spawnPoint = LocationManager.locationFromConfig(s, true);

		state = ArenaState.WAITING;
	}

	public int getID() {
		return id;
	}

	public Location getSpawnPoint() {
		return spawnPoint;
	}
	
	public ArenaState getState() {
		return state;
	}

	public int getCurrentPlayers() {
		return currentPlayers;
	}

	public void addPlayer(Player p) {
		if (currentPlayers >= numPlayers) {
			MessageManager.getInstance().msg(p, MessageType.BAD, "There are too many players in that arena already!");
			return;
		}

		if (spawnPoint == null) {
			MessageManager.getInstance().msg(p, MessageType.BAD, "The spawn point for this arena has not been set yet.");
			return;
		}

		data.add(new PlayerData(p));
		
		p.getInventory().clear();

		p.getInventory().setItem(4, new ItemStack(classselect));
		
		currentPlayers++;
		
		GameListener.getInstance().getPlayerKillsMap().put(p.getName(), new Integer(0));
		
		p.setGameMode(GameMode.SURVIVAL);
		
		p.teleport(spawnPoint);
		
		p.setFoodLevel(20);
		p.setHealth(20);
		
		GoldenGun.perms.playerAdd(p, "-crackshot.use.all");
		GoldenGun.perms.playerRemove(p, "crackshot.use.all");
		
		sendMessage(MessageType.INFO, p.getName() + " has joined the arena! (" + currentPlayers + "/" + numPlayers + ")");
		p.sendMessage("");
		MessageManager.getInstance().msg(p, MessageType.GOOD, "Get to a safe spot before the game starts!");
		MessageManager.getInstance().msg(p, MessageType.INFO, "Use the lore to find out how your weapon works");
		MessageManager.getInstance().msg(p, MessageType.INFO, "Repeated spawn-killing will result in a ban!");
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
			public void run() {
				for (PlayerData pd : data) updateScoreboard(pd.getPlayer());
			}
		}, 60);
		
		if (currentPlayers == minPlayers) {
			start();
		}
	}
	
	public void updateScoreboard(Player p) {
		
		SimpleScoreboard ss = new SimpleScoreboard(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "Golden" + ChatColor.YELLOW + "Gun" + ChatColor.DARK_GRAY + "]");
		ss.add("===================");
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "Current Map:");
		ss.add("" + mapName);
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "Current Players:");
		ss.add("" + currentPlayers);
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "Kills:");
		ss.add("" + GameListener.getInstance().getPlayerKillsMap().get(p.getName()));
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "Phase:");
		ss.add("Alpha");
		ss.blankLine();
		
		ss.add("===================");
		ss.build();

		ss.send(p);
	}
	
	public void removePlayer(Player p) {
		PlayerData d = getPlayerData(p);
		data.remove(d);
		
		currentPlayers--;
		
		GoldenGun.perms.playerRemove(p, "-crackshot.use.all");
		GoldenGun.perms.playerAdd(p, "crackshot.use.all");

		if (currentPlayers != 0) {
			for (PlayerData pd : data) updateScoreboard(pd.getPlayer());
		}
	}
	
	public void playerLeave(Player p) {
		PlayerData d = getPlayerData(p);
		d.restorePlayer();
		data.remove(d);
		
		currentPlayers--;
		
		GoldenGun.perms.playerRemove(p, "-crackshot.use.all");
		GoldenGun.perms.playerAdd(p, "crackshot.use.all");

		if (state == ArenaState.STARTED) {
			
		} else {
			
		}
	}
	
	@SuppressWarnings("deprecation")
	public void start() {
		Random r = new Random();
		
		final Integer currentPlayerData = currentPlayers - 1;
		final Integer randomPlayer = r.nextInt(currentPlayerData);
		
		final Countdown c = new Countdown(30, "The game is starting in %t seconds!", "beginning", this, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown gameEnd = new Countdown(300, "The game is ending in %t seconds!", "ending", this, 300, 240, 180, 120, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown lobbyTeleport = new Countdown(5, "You will be teleported to the lobby in %t seconds!", "ended", this, 5, 4, 3, 2, 1);
		
		taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
			public void run() {
				if (currentPlayers >= minPlayers) {
					if (!c.isDone()) {
						c.run();
						
						state = ArenaState.COUNTING_DOWN;
					} else {
						Bukkit.getServer().getScheduler().cancelTask(taskID);
						
						state = ArenaState.STARTED;
						
						for (PlayerData pd : data) {
							GoldenGun.perms.playerAdd(pd.getPlayer(), "crackshot.use.all");
							GoldenGun.perms.playerRemove(pd.getPlayer(), "-crackshot.use.all");
						}
						
						csu.giveWeapon(data.get(randomPlayer).getPlayer(), "GoldenGun", 1);
						
						taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
							public void run() {
								if (currentPlayers != 0) {
									if (!gameEnd.isDone()) {
										gameEnd.run();
										
										state = ArenaState.STARTED;
									} else {
										Bukkit.getServer().getScheduler().cancelTask(taskID);
										
										state = ArenaState.FINISHING;
										
										ValueComparator bvc = new ValueComparator(GameListener.getInstance().getPlayerKillsMap());
										final TreeMap<String, Integer> sortedPlayerKills = new TreeMap<String, Integer>(bvc);
										
										sortedPlayerKills.putAll(GameListener.getInstance().getPlayerKillsMap());
										final List<String> playersInGame = new ArrayList<String>();
										playersInGame.addAll(sortedPlayerKills.keySet());
										
										stop(Bukkit.getPlayer(playersInGame.get(0)));
										
										Logger.getLogger("Minecraft").info("Arena " + id + " has ended!");
										
										taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
											public void run() {
												if (!lobbyTeleport.isDone()) {
													lobbyTeleport.run();
													
													state = ArenaState.FINISHING;
												} else {
													Bukkit.getServer().getScheduler().cancelTask(taskID);
													
													state = ArenaState.WAITING;
													
													stop(Bukkit.getPlayer(playersInGame.get(0)));
													
													taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {

														@Override
														public void run() {
															if (currentPlayers != 0) {
																data.get(currentPlayers - 1).restorePlayer();
																removePlayer(data.get(currentPlayers - 1).getPlayer());
															} else {
																Bukkit.getServer().getScheduler().cancelTask(taskID);
																
																GoldenGun.getPlugin().getLogger().info("Arena " + id + " has completely ended!");
															}
														}
														
													}, 0, 5);
													
													playersInGame.clear();
													sortedPlayerKills.clear();
													
													stop();
												}
											}
										}, 0, 20);
									}
								} else {
									state = ArenaState.WAITING;
									
									Bukkit.getServer().getScheduler().cancelTask(taskID);
								}
							}
						}, 0, 20);
					}
				} else {
					Bukkit.getServer().getScheduler().cancelTask(taskID);
					
					sendMessage(MessageType.INFO, "Countdown cancelled, someone left");
					
					state = ArenaState.WAITING;
				}
			}
		}, 0, 20);
	}
	
	public void forceStart() {
		Random r = new Random();
		
		final Integer currentPlayerData = currentPlayers - 1;
		final Integer randomPlayer = r.nextInt(currentPlayerData);
		
		final Countdown c = new Countdown(30, "The game is starting in %t seconds!", "beginning", this, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown gameEnd = new Countdown(300, "The game is ending in %t seconds!", "ending", this, 300, 240, 180, 120, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown lobbyTeleport = new Countdown(5, "You will be teleported to the lobby in %t seconds!", "ended", this, 5, 4, 3, 2, 1);
		
		this.taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
			public void run() {
				if (!c.isDone()) {
					c.run();
					state = ArenaState.COUNTING_DOWN;
				} else { 
					Bukkit.getServer().getScheduler().cancelTask(taskID);
					state = ArenaState.STARTED;
					for (PlayerData pd : data) GoldenGun.perms.playerAdd(pd.getPlayer(), "crackshot.use.all");
					for (PlayerData pd : data) GoldenGun.perms.playerRemove(pd.getPlayer(), "-crackshot.use.all");
					csu.giveWeapon(data.get(randomPlayer).getPlayer(), "GoldenGun", 1);
					taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
						@SuppressWarnings("deprecation")
						public void run() {						
							if (currentPlayers != 0) {
								if(!gameEnd.isDone()) {
									gameEnd.run();
									
									state = ArenaState.COUNTING_DOWN;
								} else {
									Bukkit.getServer().getScheduler().cancelTask(taskID);
									
									ValueComparator bvc = new ValueComparator(GameListener.getInstance().getPlayerKillsMap());
									final TreeMap<String, Integer> sortedPlayerKills = new TreeMap<String, Integer>(bvc);
							
									sortedPlayerKills.putAll(GameListener.getInstance().getPlayerKillsMap());
									final List<String> playersInGame = new ArrayList<String>();
									playersInGame.addAll(sortedPlayerKills.keySet());

									Logger.getLogger("Minecraft").info("Arena " + id + " has ended!");
									
									taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {
										public void run() {
											if (!lobbyTeleport.isDone()) {
												lobbyTeleport.run();
												
												state = ArenaState.FINISHING;
											} else {
												Bukkit.getServer().getScheduler().cancelTask(taskID);
												
												state = ArenaState.WAITING;
												
												stop(Bukkit.getPlayer(playersInGame.get(0)));
												
												taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GoldenGun.getPlugin(), new Runnable() {

													@Override
													public void run() {
														if (currentPlayers != 0) {
															data.get(currentPlayers - 1).restorePlayer();
															removePlayer(data.get(currentPlayers - 1).getPlayer());
														} else {
															Bukkit.getServer().getScheduler().cancelTask(taskID);
															
															GoldenGun.getPlugin().getLogger().info("Arena " + id + " has completely ended!");
														}
													}
													
												}, 0, 5);
												
												playersInGame.clear();
												sortedPlayerKills.clear();
												
											}
										}
									}, 0, 20);
								}
							} else {						
								Bukkit.getServer().getScheduler().cancelTask(taskID);
									
								state = ArenaState.WAITING;
							}
						}
					}, 0, 20);
				}
			}
		}, 0, 20);
	}

	@SuppressWarnings("deprecation")
	public void stop(Player winner) {
		if (winner != null) sendMessage(MessageType.GOOD, winner.getName() + " has won the game!");
		
		GoldenGun.econ.depositPlayer(winner.getName(), 10);
		
		MessageManager.getInstance().msg(winner, MessageType.GOOD, "You have won the game and 10 dogtags!", "Your new balance is " + GoldenGun.econ.getBalance(winner.getName()) + "!");
	}
	
	public void stop() {
		
		GoldenGun.getPlugin().getLogger().info("Arena " + id + " has finished successfully");
		
		this.state = ArenaState.WAITING;
	}
    
	public boolean containsPlayer(Player p) {
		return getPlayerData(p) != null;
	}

	public PlayerData getPlayerData(Player p) {
		for (PlayerData d : data) {
			if (d.isForPlayer(p)) return d;
		}
		return null;
	}
	
	public Player getRandomPlayer() {
		Random r = new Random();
		Integer i = r.nextInt(currentPlayers) + 1;
		return data.get(i).getPlayer();
	}

	public void sendMessage(MessageType type, String... messages) {
		for (PlayerData d : data) MessageManager.getInstance().msg(d.getPlayer(), type, messages);
	}
}

class PlayerData {

	public static ScoreboardListener scorelisten = new ScoreboardListener();
	
	private String playerName;
	private ItemStack[] contents, armorContents;
	private GameMode gm;
	private Location spawn;

	protected PlayerData(Player p) {
		this.playerName = p.getName();
		this.contents = p.getInventory().getContents();
		this.armorContents = p.getInventory().getArmorContents();
		this.gm = p.getGameMode();
		this.spawn = Bukkit.getWorld("Lobby").getSpawnLocation();
	}
	
	@SuppressWarnings("deprecation")
	protected Player getPlayer() {
		return Bukkit.getServer().getPlayer(playerName);
	}

	@SuppressWarnings("deprecation")
	protected void restorePlayer() {
		Player p = Bukkit.getServer().getPlayer(playerName);

		if (GameListener.getInstance().getPlayerKillsMap().containsKey(playerName)) {
			GameListener.getInstance().getPlayerKillsMap().remove(playerName);
		}
		
		if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().containsKey(playerName)) {
			ClassSelectInventoryListener.getInstance().getPlayerKitMap().remove(playerName);
		}
		
		scorelisten.sendScoreboardToPlayer(p);
		
		p.getInventory().setContents(contents);
		p.getInventory().setArmorContents(armorContents);
		p.setGameMode(gm);
		p.teleport(spawn);
	}

	protected boolean isForPlayer(Player p) {
		return playerName.equalsIgnoreCase(p.getName());
	}
}

class Countdown implements Runnable {

	private boolean isDone = false;
	private int timer;
	private String msg, type;
	private Arena a;
	private ArrayList<Integer> countingNums;

	public Countdown(int start, String msg, String type, Arena a, int... countingNums) {
		this.timer = start;
		this.msg = msg;
		this.type = type;
		this.a = a;
		this.countingNums = new ArrayList<Integer>();
		for (int i : countingNums) this.countingNums.add(i);
	}

	public Countdown(int i, String string, Runnable runnable, int j, int k,
			int l, int m, int n, int o, int p, int q, int r, int s) {
		
	}

	public void run() {
		if (timer == 0) {
			a.sendMessage(MessageType.GOOD, "The game is now " + type);
			isDone = true;
			return;
		}

		if (countingNums.contains(timer)) {
			a.sendMessage(MessageType.INFO, msg.replaceAll("%t", timer + ""));
		}

		timer--;
	}

	public boolean isDone() {
		return isDone;
	}
}
