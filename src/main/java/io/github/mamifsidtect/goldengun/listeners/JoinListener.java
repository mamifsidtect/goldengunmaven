package io.github.mamifsidtect.goldengun.listeners;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.Arena.ArenaState;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinListener implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		GoldenGun.perms.playerAdd(p, "crackshot.use.all");
		GoldenGun.perms.playerRemove(p, "-crackshot.use.all");
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		if (ArenaManager.getInstance().getArena(p) != null) {
			if (ArenaManager.getInstance().getArena(p).getState() != ArenaState.STARTED) {
				ArenaManager.getInstance().getArena(p).removePlayer(p);
			} else if (ArenaManager.getInstance().getArena(p).getState() == ArenaState.STARTED) {
				ArenaManager.getInstance().getArena(p).playerLeave(p);
			}
		}
	}
}
