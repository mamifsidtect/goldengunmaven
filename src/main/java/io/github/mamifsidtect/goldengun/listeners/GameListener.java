package io.github.mamifsidtect.goldengun.listeners;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.shampaggon.crackshot.CSUtility;

public class GameListener implements Listener {

	public final CSUtility csu = new CSUtility();
	private static GameListener instance = new GameListener();

	public static GameListener getInstance() {
		return instance;
	}

	private static HashMap<String, Integer> playerKills = new HashMap<String, Integer>();

	public HashMap<String, Integer> getPlayerKillsMap() {
		return playerKills;
	}

	@EventHandler
	public void onPlayerKill(PlayerDeathEvent event) {
		Player player = event.getEntity();
		Player killer = event.getEntity().getKiller();
		
		if (ArenaManager.getInstance().getArena(player) != null) {
			if (event.getEntity() instanceof Player) {
				if (killer != player) {
					Integer newKills = new Integer(getPlayerKillsMap().get(killer.getName()) + 1);
					getPlayerKillsMap().put(killer.getName(), newKills);
					ArenaManager.getInstance().getArena(killer).updateScoreboard(killer);
					ArenaManager.getInstance().getArena(killer).sendMessage(MessageType.INFO, player.getName() + " was killed by " + killer.getName(), killer.getName() + " now has " + getPlayerKillsMap().get(killer.getName()) + " kills!");
				} else if (killer == player) {
					Integer suicideLoss = new Integer(getPlayerKillsMap().get(player.getName()) - 1);
					getPlayerKillsMap().put(player.getName(), suicideLoss);
					ArenaManager.getInstance().getArena(player).updateScoreboard(player);
					ArenaManager.getInstance().getArena(killer).sendMessage(MessageType.INFO, player.getName() + " killed themselves!", player.getName() + " now has " + getPlayerKillsMap().get(player.getName()) + " kills!");
					MessageManager.getInstance().msg(player, MessageType.INFO, "You killed yourself. For that, you lose 1 kill");
				}
			}
		}
	}

	@EventHandler
	public void fixKillerInventory(PlayerDeathEvent event) {
		Player player = event.getEntity();
		Player killer = player.getKiller();
		EntityDamageEvent ede = player.getLastDamageCause();
		DamageCause dc = ede.getCause();

        if (player.getInventory().contains(Material.GOLD_BARDING) && killer != player) {
            csu.giveWeapon(killer, "GoldenGun", 1);
            return;
        }

        if (player.getInventory().contains(Material.GOLD_BARDING)) {
            if (dc.equals(DamageCause.VOID)
                    || dc.equals(DamageCause.LAVA)
                    || dc.equals(DamageCause.FIRE)
                    || dc.equals(DamageCause.FIRE_TICK)
                    || dc.equals(DamageCause.FALL)) {
                csu.giveWeapon(ArenaManager.getInstance().getArena(killer).getRandomPlayer(), "GoldenGun", 1);
            }
        }
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Entity e = event.getEntity();
		if (ArenaManager.getInstance().getArena((Player) e).containsPlayer((Player) e)) {
			event.setDeathMessage(null);
		}
	}
	
	//@EventHandler (priority = EventPriority.HIGHEST)
	//public void onPlayerRespawnLocation(PlayerRespawnEvent event) {
	//	Random random = new Random();
	//	Player p = event.getPlayer();
	//	if (random.nextInt(16) + 1 == 1) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint1());
	//	} else if (random.nextInt(16) + 1 == 2) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint2());
	//	} else if (random.nextInt(16) + 1 == 3) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint3());
	//	} else if (random.nextInt(16) + 1 == 4) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint4());
	//	} else if (random.nextInt(16) + 1 == 5) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint5());
	//	} else if (random.nextInt(16) + 1 == 6) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint6());
	//	} else if (random.nextInt(16) + 1 == 7) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint7());
	//	} else if (random.nextInt(16) + 1 == 8) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint8());
	//	} else if (random.nextInt(16) + 1 == 9) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint9());
	//	} else if (random.nextInt(16) + 1 == 10) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint10());
	//	} else if (random.nextInt(16) + 1 == 11) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint11());
	//	} else if (random.nextInt(16) + 1 == 12) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint12());
	//	} else if (random.nextInt(16) + 1 == 13) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint13());
	//	} else if (random.nextInt(16) + 1 == 14) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint14());
	//	} else if (random.nextInt(16) + 1 == 15) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint15());
	//	} else if (random.nextInt(16) + 1 == 16) {
	//		event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint16());
	//	}
	//}

	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		final Player p = event.getPlayer();
		String assassinKit = "Assassin";
		String assaultKit = "Assault";
		String hunterKit = "Hunter";
		String sniperKit = "Sniper";
		String stalkerKit = "Stalker";
		String soldierKit = "Soldier";
		String predatorKit = "Predator";
		String marksmanKit = "Marksman";
		String sluggerKit = "Slugger";
		String heavyKit = "Heavy";
		String grenadierKit = "Grenadier";
		String tacticalKit = "Tactical";
		
		if (ArenaManager.getInstance().getArena(p) != null) {
			event.setRespawnLocation(ArenaManager.getInstance().getArena(p).getSpawnPoint());
			if (!ClassSelectInventoryListener.getInstance().getPlayerKitMap().containsKey(p.getName())) {
				p.getInventory().clear();
				csu.giveWeapon(p, "SCAR", 1);
				csu.giveWeapon(p, "Putty", 1);
				csu.giveWeapon(p, "Flashbang", 1);
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);	
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(assassinKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "XM500", 1);
				csu.giveWeapon(p, "Putty", 1);
				csu.giveWeapon(p, "Flashbang", 2);
				csu.giveWeapon(p, "Shock", 1);
				csu.giveWeapon(p, "Python", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(assaultKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "SCAR", 1);
				csu.giveWeapon(p, "Grenade", 2);
				csu.giveWeapon(p, "Flashbang", 2);
				csu.giveWeapon(p, "M1911", 1);			
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(grenadierKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "Launcher", 1);
				csu.giveWeapon(p, "Flashbang", 2);
				csu.giveWeapon(p, "Shock", 2);
				csu.giveWeapon(p, "M1911", 1);			
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(hunterKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "M1014", 1);
				csu.giveWeapon(p, "Grenade", 2);
				csu.giveWeapon(p, "Shock", 1);
				csu.giveWeapon(p, "M1911", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(sniperKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "Winchester", 1);
				csu.giveWeapon(p, "Grenade", 2);
				csu.giveWeapon(p, "Flashbang", 1);
				csu.giveWeapon(p, "Eagle", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(stalkerKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "Olympia", 1);
				csu.giveWeapon(p, "Shock", 1);
				csu.giveWeapon(p, "Flashbang", 1);
				csu.giveWeapon(p, "Grenade", 2);
				csu.giveWeapon(p, "Python", 1);		
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(soldierKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "AK47", 1);
				csu.giveWeapon(p, "Flashbang", 1);
				csu.giveWeapon(p, "Shock", 2);
				csu.giveWeapon(p, "Putty", 1);
				csu.giveWeapon(p, "Python", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(predatorKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "SPAS12", 1);
				csu.giveWeapon(p, "Shock", 1);
				csu.giveWeapon(p, "Grenade", 2);
				csu.giveWeapon(p, "Eagle", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(marksmanKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "SV98", 1);
				csu.giveWeapon(p, "Putty", 1);
				csu.giveWeapon(p, "Flashbang", 1);
				csu.giveWeapon(p, "Python", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(sluggerKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "M26", 1);
				csu.giveWeapon(p, "Handcannon", 1);
				csu.giveWeapon(p, "Flashbang", 1);
				csu.giveWeapon(p, "Shock", 1);
				csu.giveWeapon(p, "Grenade", 2);			
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(heavyKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "Minigun", 1);
				csu.giveWeapon(p, "M1911", 1);
				csu.giveWeapon(p, "Shock", 2);
				csu.giveWeapon(p, "Grenade", 2);			
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			} else if (ClassSelectInventoryListener.getInstance().getPlayerKitMap().get(p.getName()).equals(tacticalKit)) {
				p.getInventory().clear();
				csu.giveWeapon(p, "M16", 1);
				csu.giveWeapon(p, "Flashbang", 2);
				csu.giveWeapon(p, "Shock", 2);
				csu.giveWeapon(p, "Grenade", 1);
				csu.giveWeapon(p, "Eagle", 1);				
				Bukkit.getScheduler().scheduleSyncDelayedTask(GoldenGun.getPlugin(), new Runnable() {
					public void run() {
						ArenaManager.getInstance().getArena(p).updateScoreboard(p);
					}
				}, 60);
			}
		} else {
			event.setRespawnLocation(Bukkit.getWorld("Lobby").getSpawnLocation());
		}
	}
}
