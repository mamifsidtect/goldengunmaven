package io.github.mamifsidtect.goldengun.commands;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.ConfigManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SetGameSpawns extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (GoldenGun.perms.playerHas(p, "goldengun.commands.setspawns")) {
			if (args.length == 0) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You did not specify an arena ID or a respawn point ID!");
				return;
			} else if (args.length == 1) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You did not specify a respawn point ID!");
				return;
			}
			
			int id = -1;
			int spawnID = -1;
			
			try { id = Integer.parseInt(args[0]); }
			catch (Exception e) { 
				MessageManager.getInstance().msg(p, MessageType.BAD, args[0] + " is not a valid number!");
				return;
			}
			
			try { spawnID = Integer.parseInt(args[1]); }
			catch (Exception e) {
				MessageManager.getInstance().msg(p, MessageType.BAD, args[1] + " is not a valid number for respawn point!");
				return;
			}
			
			ConfigurationSection s = ConfigManager.getArenas().createConfigurationSection("arenas." + id + ".respawns." + spawnID);
			
			s.set("world", p.getWorld().getName());
			s.set("x", p.getLocation().getX());
			s.set("y", p.getLocation().getY());
			s.set("z", p.getLocation().getY());
			s.set("yaw", p.getLocation().getYaw());
			s.set("pitch", p.getLocation().getPitch());
			
			ConfigManager.getArenas().set("arenas." + id + ".respawns." + spawnID, s);
			
			ArenaManager.getInstance().setupArenas();
			
			MessageManager.getInstance().msg(p, MessageType.GOOD, "Set respawn number " + spawnID + " successfully!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}
	
	public SetGameSpawns() {
		super("Sets the spawn of the number that you specify in an arena.", "<arenaID> <spawnID>", "sgs");
	}
}
