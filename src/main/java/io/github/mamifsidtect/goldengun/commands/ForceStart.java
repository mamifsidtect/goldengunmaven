package io.github.mamifsidtect.goldengun.commands;

import io.github.mamifsidtect.goldengun.GoldenGun;
import io.github.mamifsidtect.goldengun.managers.Arena;
import io.github.mamifsidtect.goldengun.managers.Arena.ArenaState;
import io.github.mamifsidtect.goldengun.managers.ArenaManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager;
import io.github.mamifsidtect.goldengun.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class ForceStart extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (GoldenGun.perms.playerHas(p, "goldengun.commands.forcestart")) {
			if (args.length == 0) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You must specify an arena ID.");
				return;
			}

			int id = -1;

			try { id = Integer.parseInt(args[0]); }
			catch (Exception e) {
				MessageManager.getInstance().msg(p, MessageType.BAD, args[0] + " is not a valid number!");
				return;
			}

			Arena a = ArenaManager.getInstance().getArena(id);

			if (a == null) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "There is no arena with ID " + id + "!");
				return;
			}

			if (a.getState() == ArenaState.STARTED) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "Arena " + id + " has already started!");
				return;
			}

			a.forceStart();
			MessageManager.getInstance().msg(p, MessageType.GOOD, "Force started arena " + a.getID() + "!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public ForceStart() {
		super("Force start an arena.", "<id>", "start");
	}
}
